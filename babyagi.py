#!/usr/bin/env python3
import os
import openai
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import Chroma
import time
import sys
from collections import deque
from typing import Dict, List
from dotenv import load_dotenv

load_dotenv()

# Constants
API_KEYS = {
    "OPENAI_API_KEY": os.getenv("OPENAI_API_KEY", ""),
    "OPENAI_API_MODEL": os.getenv("OPENAI_API_MODEL", "text-davinci-003"),
}
YOUR_TABLE_NAME = os.getenv("TABLE_NAME", "")
OBJECTIVE = sys.argv[1] if len(sys.argv) > 1 else os.getenv("OBJECTIVE", "")
YOUR_FIRST_TASK = os.getenv("FIRST_TASK", "")

assert all(API_KEYS.values()), "Some API keys are missing in your .env file."
assert YOUR_TABLE_NAME, "TABLE_NAME is missing in your .env file."
assert OBJECTIVE, "OBJECTIVE is missing in your .env file."
assert YOUR_FIRST_TASK, "FIRST_TASK is missing in your .env file."

# Helper Functions
def log(message, out="log_file.txt", silent=False):
    if not silent:
        print(message)
    with open(out, "a") as log_file:
        log_file.write(str(message) + "\n")

def configure_openai(api_key, model_name):
    openai.api_key = api_key
    return model_name.startswith("gpt-")

USE_CHAT_COMPLETIONS = configure_openai(API_KEYS["OPENAI_API_KEY"], API_KEYS["OPENAI_API_MODEL"])

# Create Chroma index
embeddings = OpenAIEmbeddings()
db = None

# Task list
task_list = deque([])

def add_task(task: Dict):
    task_list.append(task)

def get_ada_embedding(text):
    text = text.replace("\n", " ")
    return openai.Embedding.create(input=[text], model="text-embedding-ada-002")["data"][0]["embedding"]

def openai_call(prompt: str, temperature: float = 0.5, max_tokens: int = 200):
    if not USE_CHAT_COMPLETIONS:
        # Use completion API
        response = openai.Completion.create(
            engine=API_KEYS["OPENAI_API_MODEL"],
            prompt=prompt,
            temperature=temperature,
            max_tokens=max_tokens,
            top_p=1,
            frequency_penalty=0,
            presence_penalty=0
        )
        return response.choices[0].text.strip()
    else:
        # Use chat completion API
        messages=[{"role": "user", "content": prompt}]
        response = openai.ChatCompletion.create(
            model=API_KEYS["OPENAI_API_MODEL"],
            messages=messages,
            temperature=temperature,
            max_tokens=max_tokens,
            n=1,
            stop=None,
        )
        return response.choices[0].message.content.strip()

def gpt4_call(prompt: str, temperature: float = 0.5, max_tokens: int = 200):
    messages=[{"role": "user", "content": prompt}]
    response = openai.ChatCompletion.create(
        model="gpt-4",
        messages=messages,
        temperature=temperature,
        max_tokens=max_tokens,
        n=1,
        stop=None,
    )
    return response.choices[0].message.content.strip()

def task_creation_agent(objective: str, result: Dict, task_description: str, task_list: List[str]):
    prompt = f"You are a task creation AI that uses the result of an execution agent to create new tasks with the following objective: {objective}, The last completed task has the result: {result}. This result was based on this task description: {task_description}. These are incomplete tasks: {', '.join(task_list)}. Based on the result, create new tasks to be completed by the AI system that do not overlap with incomplete tasks. Return the tasks as an array."
    response = openai_call(prompt)
    new_tasks = response.split('\n')
    return [{"task_name": task_name} for task_name in new_tasks]

def prioritization_agent(this_task_id:int):
    global task_list
    task_names = [t["task_name"] for t in task_list]
    next_task_id = int(this_task_id)+1
    prompt = f"""You are a task prioritization AI tasked with cleaning the formatting of and reprioritizing the following tasks: {task_names}. Consider the ultimate objective of your team:{OBJECTIVE}. Do not remove any tasks. Return the result as a numbered list, like:
    #. First task
    #. Second task
    Start the task list with number {next_task_id}."""
    response = openai_call(prompt)
    new_tasks = response.split('\n')
    task_list = deque()
    for task_string in new_tasks:
        task_parts = task_string.strip().split(".", 1)
        if len(task_parts) == 2:
            task_id = task_parts[0].strip()
            task_name = task_parts[1].strip()
            task_list.append({"task_id": task_id, "task_name": task_name})

def execution_agent(objective:str,task: str) -> str:
    context = None
    if db:
        context=context_agent(query=objective, n=5)
    prompt =f"You are an AI who performs one task based on the following objective: {objective}.\nTake into account these previously completed tasks: {context}\nYour task: {task}\nResponse:"
    return openai_call(prompt, 0.7, 2000)

def context_agent(query: str, n: int):
    nmax = min(n, db._collection.count())
    try:
        results = db.similarity_search(query, k=nmax)
    except Exception as ee:
        print("Error: ", ee)
        results = []
    return results

def synthesize_completed_tasks(task_holder: Dict):
    completed_tasks = [f"Task {task['task_id']}: {task['task_name']} -> Result: {result}" for task_id, task_info in task_holder.items() for task, result in [(task_info['task'], task_info['result'])]]
    prompt = f"Using the following information create a detailed flowchart with descriptions about the best approach and information needed to make an informed decision about choosing the best medicare plan:\n{chr(10).join(completed_tasks)}"
    log("OUTPUT PROMPT")
    log(prompt)
    conclusion = gpt4_call(prompt, 0, 6000)
    return conclusion

# Add the first task
first_task = {
    "task_id": 1,
    "task_name": YOUR_FIRST_TASK
}

add_task(first_task)
# Main loop
task_holder = {}
task_id_counter = 1
loop_counter = 0

#

while True:
    if task_list:
        # Log the task list
        log("\033[95m\033[1m"+"\n*****TASK LIST*****\n"+"\033[0m\033[0m")
        for t in task_list:
            log(str(t['task_id'])+": "+t['task_name'])

        # Step 1: Pull the first task
        task = task_list.popleft()
        log("\033[92m\033[1m"+"\n*****NEXT TASK*****\n"+"\033[0m\033[0m")
        log(str(task['task_id'])+": "+task['task_name'])

        # Send to execution function to complete the task based on the context
        result = execution_agent(OBJECTIVE,task["task_name"])
        this_task_id = int(task["task_id"])
        task_holder[task_id_counter] = {'task': task, 'result': result}
        log("\033[93m\033[1m"+"\n*****TASK RESULT*****\n"+"\033[0m\033[0m")
        log(result)

         # Step 2: Enrich result and store in Pinecone / Chroma
        result_id = f"result_{task['task_id']}"
        enriched_result = {'data': result}
        # Chroma
        if not db:
            db = Chroma.from_texts([result], metadatas=[{'task_name': task['task_name']}], ids=[result_id], embedding = embeddings)
        else:
            db.add_texts([result], metadatas=[{'task_name': task['task_name']}], ids=[result_id])

    # Step 3: Create new tasks and reprioritize task list
    new_tasks = task_creation_agent(OBJECTIVE,enriched_result, task["task_name"], [t["task_name"] for t in task_list])

    for new_task in new_tasks:
        task_id_counter += 1
        new_task.update({"task_id": task_id_counter})
        add_task(new_task)

    prioritization_agent(this_task_id)
    loop_counter += 1
    if loop_counter > 5:
        break
    time.sleep(1)  # Sleep before checking the task list again


tout = "task.txt"
for dic in task_holder.values():
    task = dic["task"]
    result = dic["result"]

    log("Task: "+str(task["task_id"]), tout, silent=True)
    log(task["task_name"], tout, silent=True)
    log(result, tout, silent=True)

# Synthesize a conclusion from the completed tasks
conclusion = synthesize_completed_tasks(task_holder)
log("\033[94m\033[1m" + "\n*****SYNTHESIZED CONCLUSION*****\n" + "\033[0m\033[0m")
log(conclusion, "conclusion.txt")
